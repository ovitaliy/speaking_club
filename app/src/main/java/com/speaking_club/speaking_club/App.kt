package com.speaking_club.speaking_club

import android.app.Application
import com.speaking_club.speaking_club.di.components.AppComponent
import com.speaking_club.speaking_club.di.components.DaggerAppComponent
import com.speaking_club.speaking_club.di.modules.AppModule

/**
 * Created by ovitaliy on 13.04.2017.
 */

class App : Application() {
    lateinit var component: AppComponent

    protected fun initDaggerComponent(): AppComponent {
        return DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

    override fun onCreate() {
        super.onCreate()
        this.component = initDaggerComponent()
    }
}
