package com.speaking_club.speaking_club.utils

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

/**
 * Created by ovitaliy on 18.04.2017.
 */

fun <T> config(observable: Observable<T>): Observable<T> {
    return observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

class TrustManager constructor(): X509TrustManager{
    @Throws(CertificateException::class)
    override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
    }

    @Throws(CertificateException::class)
    override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
    }

    override fun getAcceptedIssuers(): Array<X509Certificate?> {
        val cArrr = arrayOfNulls<X509Certificate?>(0)
        return cArrr
    }
}

fun getTrustManager(): X509TrustManager {
    return TrustManager()
}

fun getTrustedSSLContext(): SSLContext {
    val sc = SSLContext.getInstance("SSL");
    sc.init(null, arrayOf<TrustManager>(getTrustManager()), null)
    return sc;
}
