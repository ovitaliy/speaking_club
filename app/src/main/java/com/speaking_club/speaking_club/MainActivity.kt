package com.speaking_club.speaking_club

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import com.speaking_club.speaking_club.app.chat.ChatService
import com.speaking_club.speaking_club.app.chat.ChatViewImpl


class MainActivity : AppCompatActivity() {

    private var chatService: ChatService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val chatView = ChatViewImpl()
        chatService = ChatService(chatView, "1", (System.currentTimeMillis().toString()), "http://192.168.1.45:8181/chats/websocket")
        val chatMessage = findViewById(R.id.chatMessage) as TextView
        chatMessage.setOnEditorActionListener({ v, actionId, event ->
            chatService!!.postMessage(v.text.toString());
            true
        })

        (findViewById(R.id.chatMessageSend) as Button).setOnClickListener { v->
            chatService!!.postMessage((findViewById(R.id.chatMessage) as TextView).text.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (chatService != null) {
            chatService!!.disconnect()
            chatService = null;
        }
    }


}
