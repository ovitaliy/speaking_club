package com.speaking_club.speaking_club.data

/**
 * Created by ovitaliy on 10.04.2017.
 */
data class ChatMessage(val id: Long, val text: String, val userId: Long)