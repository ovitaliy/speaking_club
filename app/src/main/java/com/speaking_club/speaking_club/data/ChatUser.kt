package com.speaking_club.speaking_club.data

/**
 * Created by ovitaliy on 10.04.2017.
 */
data class ChatUser(val id: Long, val name: String, val photo: String);