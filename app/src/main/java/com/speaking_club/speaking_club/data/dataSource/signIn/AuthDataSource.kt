package com.speaking_club.speaking_club.data.dataSource.signIn

import io.reactivex.Observable

/**
 * Created by ovitaliy on 15.04.2017.
 */

interface AuthDataSource {

    fun signIn(username: String, password: String): Observable<Boolean>
    fun signUp(username: String, password: String): Observable<Boolean>

}
