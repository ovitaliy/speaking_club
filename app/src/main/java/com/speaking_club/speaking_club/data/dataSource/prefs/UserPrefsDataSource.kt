package com.speaking_club.speaking_club.data.dataSource.prefs

import android.content.SharedPreferences
import android.text.TextUtils
import com.google.common.base.Strings
import io.reactivex.Observable

class UserPrefsDataSource(sharedPreferences: SharedPreferences) : BasePrefSource(sharedPreferences) {

    var userId: String?
        get() = getStringPreference(USER_ID)
        set(value) = setStringPreference(USER_ID, value!!)

    val isAuthorized: Boolean
        get() = !Strings.isNullOrEmpty(authToken)

    val isAuthorizedAsObservable: Observable<Boolean>
        get() = Observable.just(isAuthorized)

    var authToken: String?
        get() {
            return getStringPreference(AUTH_TOKEN)
        }
        set(value) { setStringPreference(AUTH_TOKEN, value)}


    fun logout() {
        userId = null
        authToken = null
        sharedPreference.edit().clear().apply()
    }

    companion object {
        private val USER_ID = "user_id"
        private val AUTH_TOKEN = "auth_token"
    }

}
