package com.speaking_club.speaking_club.data.dataSource.signIn

/**
 * Created by ovitaliy on 15.04.2017.
 */
data class SignInRequest(val username: String, val password: String)