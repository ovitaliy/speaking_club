package com.speaking_club.speaking_club.data.dataSource.signIn

import com.speaking_club.speaking_club.data.dataSource.prefs.UserPrefsDataSource
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ovitaliy on 15.04.2017.
 */
class AuthRepository @Inject constructor(private val authDataSource: AuthRemoteDataSource, private val userPreferences: UserPrefsDataSource) : AuthDataSource {

    override fun signIn(username: String, password: String): Observable<Boolean> {
        return wrap(authDataSource.signIn(SignInRequest(username, password)))
    }

    override fun signUp(username: String, password: String): Observable<Boolean> {
        return wrap(authDataSource.signUp(SignInRequest(username, password)))
    }

    private fun wrap(requestObservable: Observable<SignInResponse>): Observable<Boolean> {
        return requestObservable.map { response -> response.token }
                .doOnNext { token -> userPreferences.authToken = token }
                .map { token -> token != null }
    }
}