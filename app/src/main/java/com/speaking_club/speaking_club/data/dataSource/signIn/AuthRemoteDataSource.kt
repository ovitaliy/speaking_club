package com.speaking_club.speaking_club.data.dataSource.signIn

import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by ovitaliy on 13.04.2017.
 */
interface AuthRemoteDataSource {


    @POST("auth/signIn")
    fun signIn(@Body sign: SignInRequest): Observable<SignInResponse>

    @POST("auth/signUp")
    fun signUp(@Body sign: SignInRequest): Observable<SignInResponse>


}