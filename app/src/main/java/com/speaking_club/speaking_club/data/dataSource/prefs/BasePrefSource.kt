package com.speaking_club.speaking_club.data.dataSource.prefs

import android.content.SharedPreferences
import android.support.annotation.Nullable

/**
 * Created by Uran on 18.07.2016.
 */
abstract class BasePrefSource(var sharedPreference: SharedPreferences) {


    @Nullable
    @Synchronized protected fun getStringPreference(name: String): String? {
        return sharedPreference.getString(name, null)
    }

    @Synchronized protected fun setStringPreference(name: String, value: String?) {
        val editor = sharedPreference.edit()
        editor.putString(name, value)
        editor.apply()
    }

    @Synchronized protected fun getBooleanPreference(name: String): Boolean? {
        return getBooleanPreference(name, true)
    }

    @Synchronized protected fun getBooleanPreference(name: String, def: Boolean): Boolean? {

        return sharedPreference.getBoolean(name, def)
    }

    @Synchronized protected fun setBooleanPreference(name: String, value: Boolean?) {
        val editor = sharedPreference.edit()
        editor.putBoolean(name, value!!)
        editor.apply()
    }

    @Synchronized protected fun getIntPreference(name: String): Int {
        return sharedPreference.getInt(name, -1)
    }

    @Synchronized protected fun setIntPreference(name: String, value: Int?) {
        val editor = sharedPreference.edit()
        editor.putInt(name, value!!)
        editor.apply()
    }

    @Synchronized protected fun getLongPreference(name: String): Long {
        try {
            return sharedPreference.getLong(name, -1)
        } catch (ex: Exception) {
            return sharedPreference.getInt(name, -1).toLong()
        }

    }

    @Synchronized protected fun setLongPreference(name: String, value: Long?) {
        val editor = sharedPreference.edit()
        try {
            editor.putLong(name, value!!)
        } catch (ex: Exception) {
            editor.putLong(name, -1)
        }

        editor.apply()
    }

}
