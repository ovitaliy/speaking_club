package com.speaking_club.speaking_club.di.modules

import android.content.Context
import com.google.gson.GsonBuilder
import com.speaking_club.speaking_club.Constants
import com.speaking_club.speaking_club.data.dataSource.prefs.UserPrefsDataSource
import com.speaking_club.speaking_club.utils.getTrustManager
import com.speaking_club.speaking_club.utils.getTrustedSSLContext
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import javax.inject.Singleton
import android.content.ContentValues.TAG
import android.util.Log
import java.lang.String
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession


/**
 * Created by ovi on 12/09/16.
 */
@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideRetrofit(context: Context, userPrefsDataSource: UserPrefsDataSource): Retrofit {
        val builder = OkHttpClient.Builder()

        builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

        val cacheDir = File(context.cacheDir, "cached")
        val cache = Cache(cacheDir, DISK_CACHE_SIZE.toLong())
        builder.cache(cache)

        val gson = GsonBuilder()
                .setLenient()
                .create()

        builder.sslSocketFactory(getTrustedSSLContext().socketFactory, getTrustManager())

        builder.addInterceptor { chain ->
            val original = chain.request()

            val headerContainsContentType = original.header("Content-Type") != null

            // Request customization: add request headers
            val requestBuilder = original.newBuilder()

            // if (!headerContainsContentType)
            requestBuilder.header("Content-Type", "application/json")


            if (userPrefsDataSource.authToken != null) {
                requestBuilder.header("Authorization", "Bearer " + userPrefsDataSource.authToken)
            }

            val request = requestBuilder.build()
            chain.proceed(request)
        }

        val hostnameVerifier = HostnameVerifier { hostname, session ->
            Log.d(TAG, "Trust Host :" + hostname)
            true
        }
        builder.hostnameVerifier(hostnameVerifier)


        return Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)) // TODO create custom factory
                .client(builder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

    }

    companion object {

        /**
         * 50MB cache size.
         */
        private val DISK_CACHE_SIZE = 50 * 1024 * 1024
        /**
         * requests timeout.
         */
        private val TIMEOUT = 10
    }


}
