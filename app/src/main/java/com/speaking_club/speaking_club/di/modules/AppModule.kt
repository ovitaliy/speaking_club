package com.speaking_club.speaking_club.di.modules

import android.content.Context
import android.content.SharedPreferences
import com.speaking_club.speaking_club.App
import com.speaking_club.speaking_club.data.dataSource.prefs.UserPrefsDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by ovitaliy on 13.04.2017.
 */
@Module class AppModule(private val application: App) {

    /**
     * Allow the application context to be injected but require that it be annotated with
     * [@Annotation][ForApplication] to explicitly differentiate it from an activity context.
     */
    @Provides @Singleton
    fun provideApplicationContext(): Context {
        return application
    }

    @Provides @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }


    @Provides @Singleton
    fun provideUserPrefsDataSource(sharedPreferences: SharedPreferences):UserPrefsDataSource{
        return UserPrefsDataSource(sharedPreferences);
    }

}