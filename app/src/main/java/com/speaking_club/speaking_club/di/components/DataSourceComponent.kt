package com.speaking_club.speaking_club.di.components

import com.speaking_club.speaking_club.data.dataSource.signIn.AuthRemoteDataSource

/**
 * Created by ovitaliy on 14.04.2017.
 */
interface DataSourceComponent {
    fun authRemoteDataSource(): AuthRemoteDataSource
}