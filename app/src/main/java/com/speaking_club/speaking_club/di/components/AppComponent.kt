package com.speaking_club.speaking_club.di.components

import android.content.Context
import com.speaking_club.speaking_club.data.dataSource.prefs.UserPrefsDataSource
import com.speaking_club.speaking_club.di.modules.ApiModule
import com.speaking_club.speaking_club.di.modules.AppModule
import com.speaking_club.speaking_club.di.modules.DataSourceModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by ovitaliy on 13.04.2017.
 */

@Singleton
@Component(modules = arrayOf(AppModule::class, ApiModule::class, DataSourceModule::class))
interface AppComponent : ApiComponent, DataSourceComponent {
    fun context(): Context

    fun userPrefsDataSource(): UserPrefsDataSource
}