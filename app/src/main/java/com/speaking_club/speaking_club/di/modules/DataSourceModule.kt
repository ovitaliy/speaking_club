package com.speaking_club.speaking_club.di.modules

import com.speaking_club.speaking_club.data.dataSource.signIn.AuthRemoteDataSource
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by ovitaliy on 14.04.2017.
 */
@Module
class DataSourceModule {

    @Provides
    @Singleton
    fun provideAuthRemoteDataSource(retrofit: Retrofit): AuthRemoteDataSource {
        return retrofit.create(AuthRemoteDataSource::class.java);
    }


}