package com.speaking_club.speaking_club.di.components

import retrofit2.Retrofit

/**
 * Created by ovitaliy on 14.04.2017.
 */
interface ApiComponent{
    fun retrofit():Retrofit
}