package com.speaking_club.speaking_club.di.scopes

import javax.inject.Qualifier

/**
 * Created by ovitaliy on 13.04.2017.
 */
@javax.inject.Scope
annotation class ForView