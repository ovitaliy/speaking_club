package com.speaking_club.speaking_club.app.chat

import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.schedulers.Schedulers
import okhttp3.*
import okio.ByteString
import java.util.concurrent.TimeUnit


/**
 * Created by ovitaliy on 20.04.2017.
 */

class ChatService(val view: ChatView, chatId: String, userId: String, val url: String) {

    //val headers: java.util.ArrayList<StompHeader> = java.util.ArrayList()

    private val TAG = ChatService::class.java.simpleName

    init {
        getStompClient().subscribeOn(Schedulers.newThread()).subscribe()
    }

    fun postMessage(message: String) {
        getStompClient().doOnNext { client ->
            client.send(message)
        }.subscribe()
    }

    fun disconnect() {
        if (webSocket != null) {
            webSocket!!.close(1000, null)
            webSocket = null;
        }
    }

    private var webSocket: WebSocket? = null
    fun getStompClient(): Observable<WebSocket> {
        if (webSocket == null) {
            return Observable.create {
                s ->
                WebSocketEcho(url, s).run()
            }
        } else {
            return Observable.just(webSocket)
        }
    }


    inner class WebSocketEcho(val url: String, val subscriber: ObservableEmitter<WebSocket>) : WebSocketListener() {
        fun run() {
            val client = OkHttpClient.Builder()
                    .readTimeout(0, TimeUnit.MILLISECONDS)
                    .build()

            val request = Request.Builder()
                    .url(url)
                    .build()
            client.newWebSocket(request, this)

            // Trigger shutdown of the dispatcher's executor so this process can exit cleanly.
            client.dispatcher().executorService().shutdown()
        }

        override fun onOpen(webSocket: WebSocket, response: Response) {
            this@ChatService.webSocket = webSocket
            subscriber.onNext(webSocket)
            subscriber.onComplete()

            webSocket.send("hello")
        }

        override fun onMessage(webSocket: WebSocket, text: String) {
            println("MESSAGE: " + text)
        }

        override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
            println("MESSAGE: " + bytes.hex())
        }

        override fun onClosing(webSocket: WebSocket?, code: Int, reason: String) {
            if (webSocket != null)
                webSocket.close(1000, null)
            println("CLOSE: $code $reason")
        }

        override fun onFailure(webSocket: WebSocket?, t: Throwable, response: Response) {
            t.printStackTrace()
            subscriber.onError(t)
        }
    }

}