package com.speaking_club.speaking_club.app.chat

import com.speaking_club.speaking_club.data.ChatUser

/**
 * Created by ovitaliy on 20.04.2017.
 */

interface ChatView {

    fun onChatConnected()

    fun onChatDisconnected()

    fun onChatError(throwable: Throwable)

    fun onMemberConnected(chatUser: String)

    fun onMemberDisconnected(chatUser: String)

    fun onNewMessage(message:String)

}
