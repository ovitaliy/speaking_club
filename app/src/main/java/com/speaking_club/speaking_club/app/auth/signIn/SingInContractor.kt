package com.speaking_club.speaking_club.app.auth.signIn

import com.speaking_club.speaking_club.BaseView

/**
 * Created by ovitaliy on 13.04.2017.
 */
interface SingInContractor {

    interface View : BaseView<Presenter> {

        fun authorized()

        fun showProgress(show: Boolean)

    }

    interface Presenter {
        fun signIn(username: String, password: String)
    }

}