package com.speaking_club.speaking_club.app.auth.signIn

import com.speaking_club.speaking_club.app.splash.SplashActivity
import com.speaking_club.speaking_club.di.components.AppComponent
import com.speaking_club.speaking_club.di.scopes.ForView

import dagger.Component

@ForView
@Component(modules = arrayOf(SplashPresenterModule::class), dependencies = arrayOf(AppComponent::class))
internal interface SplashComponent {

    fun inject(signInActivity: SplashActivity)

}
