package com.speaking_club.speaking_club.app.auth.signIn

import com.speaking_club.speaking_club.data.dataSource.signIn.AuthRepository
import com.speaking_club.speaking_club.utils.config
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

/**
 * Created by ovitaliy on 14.04.2017.
 */
class SignInPresenter @Inject constructor(val view: SingInContractor.View, val authRepository: AuthRepository) : SingInContractor.Presenter {

    override fun signIn(username: String, password: String) {
        view.showProgress(true)

        config(authRepository.signIn(username, password))
                .subscribeBy(
                        onNext = {
                            if (it) view.authorized()
                            view.showProgress(false)
                        },
                        onError = {
                            it.printStackTrace()
                            view.showProgress(false)
                            view.showError(it.localizedMessage)
                        }
                )
    }

    init {
        println(authRepository)
    }
}
