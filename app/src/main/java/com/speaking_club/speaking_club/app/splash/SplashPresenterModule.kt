package com.speaking_club.speaking_club.app.auth.signIn

import com.speaking_club.speaking_club.app.splash.SplashActivity
import com.speaking_club.speaking_club.app.splash.SplashContractor
import dagger.Module
import dagger.Provides

@Module
internal class SplashPresenterModule(private val view: SplashContractor.View) {

    @Provides
    fun provideSplashContractor(): SplashContractor.View {
        return view
    }

}
