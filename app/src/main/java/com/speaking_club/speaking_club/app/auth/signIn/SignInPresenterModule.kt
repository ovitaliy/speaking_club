package com.speaking_club.speaking_club.app.auth.signIn

import dagger.Module
import dagger.Provides

@Module
internal class SignInPresenterModule(private val view: SingInContractor.View) {

    @Provides
    fun provideSingInContractorView(): SingInContractor.View {
        return view
    }

}
