package com.speaking_club.speaking_club.app.splash

import com.speaking_club.speaking_club.BaseView

/**
 * Created by ovitaliy on 17.04.2017.
 */

interface SplashContractor {

    interface View : BaseView<Presenter> {

        fun redirectToAuth()

        fun redirectToMain()

    }

    interface Presenter {
        fun start()
    }

}
