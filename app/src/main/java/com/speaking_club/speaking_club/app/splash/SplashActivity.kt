package com.speaking_club.speaking_club.app.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.speaking_club.speaking_club.App
import com.speaking_club.speaking_club.MainActivity
import com.speaking_club.speaking_club.R
import com.speaking_club.speaking_club.app.auth.signIn.DaggerSplashComponent
import com.speaking_club.speaking_club.app.auth.signIn.SignInActivity
import com.speaking_club.speaking_club.app.auth.signIn.SplashPresenterModule
import javax.inject.Inject

class SplashActivity : AppCompatActivity(), SplashContractor.View {

    @Inject
    lateinit var presenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        DaggerSplashComponent.builder()
                .appComponent((application as App).component)
                .splashPresenterModule(SplashPresenterModule(this))
                .build().inject(this)

    }

    override fun showError(errorMessage: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun redirectToAuth() {
        startActivity(Intent(this, SignInActivity::class.java))
        finish()
    }

    override fun redirectToMain() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
