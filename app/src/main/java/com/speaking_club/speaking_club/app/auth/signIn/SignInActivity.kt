package com.speaking_club.speaking_club.app.auth.signIn

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.animation.AlphaAnimation
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.speaking_club.speaking_club.App
import com.speaking_club.speaking_club.MainActivity
import com.speaking_club.speaking_club.R
import javax.inject.Inject

/**
 * A signIn screen that offers signIn via email/password.
 */
class SignInActivity : AppCompatActivity(), SingInContractor.View {

    // UI references.
    @BindView(R.id.email) lateinit var emailView: AutoCompleteTextView
    @BindView(R.id.password) lateinit var passwordView: EditText
    @BindView(R.id.login_progress) lateinit var mProgressView: View
    @BindView(R.id.login_form) lateinit var mLoginFormView: View
    var loading: Boolean? = false


    @Inject
    lateinit var presenter: SignInPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        ButterKnife.bind(this)

        DaggerSignInComponent.builder()
                .appComponent((application as App).component)
                .signInPresenterModule(SignInPresenterModule(this))
                .build().inject(this)
    }

    @OnClick(R.id.email_sign_in_button)
    fun signIn() {
        presenter.signIn(emailView.text.toString(), passwordView.text.toString())
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun authorized() {
        startActivity(Intent(this, MainActivity::class.java))
    }


    private fun isEmailValid(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        return !TextUtils.isEmpty(password) && password.length > 4
    }

    /**
     * Shows the progress UI and hides the signIn form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    override fun showProgress(show: Boolean) {

        if (show) {
            mProgressView.visibility = VISIBLE
            ButterKnife.apply(mLoginFormView, ALPHA_FADE_IN)
        } else {
            mProgressView.visibility = GONE
            ButterKnife.apply(mLoginFormView, ALPHA_FADE_IN)
        }
    }


    companion object {

        private val ALPHA_FADE_IN = ButterKnife.Action<View> { view, index ->
            with(AlphaAnimation(0.5f, 1f)) {
                fillBefore = true
                duration = 2500
                view.startAnimation(this)
            }
        }
    }
}

