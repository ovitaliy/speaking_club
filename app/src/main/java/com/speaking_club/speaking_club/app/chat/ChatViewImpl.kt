package com.speaking_club.speaking_club.app.chat

/**
 * Created by ovitaliy on 20.04.2017.
 */
class ChatViewImpl : ChatView {
    override fun onChatConnected() {
        println("onChatConnected")
    }

    override fun onChatDisconnected() {
        println("onChatDisconnected")
    }

    override fun onChatError(throwable: Throwable) {
        println("onChatError" + throwable)
    }

    override fun onMemberConnected(chatUser: String) {
        println("onMemberConnected: " + chatUser)
    }

    override fun onMemberDisconnected(chatUser: String) {
        println("onMemberDisconnected: " + chatUser)
    }

    override fun onNewMessage(message: String) {
        println("message: " + message)
    }
}