package com.speaking_club.speaking_club.app.auth.signIn

import com.speaking_club.speaking_club.di.components.AppComponent
import com.speaking_club.speaking_club.di.scopes.ForView

import dagger.Component

@ForView
@Component(modules = arrayOf(SignInPresenterModule::class), dependencies = arrayOf(AppComponent::class))
internal interface SignInComponent {

    fun inject(signInActivity: SignInActivity)

}
