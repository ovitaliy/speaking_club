package com.speaking_club.speaking_club.app.splash

import com.google.common.base.Strings
import com.speaking_club.speaking_club.data.dataSource.prefs.UserPrefsDataSource
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by ovitaliy on 17.04.2017.
 */
class SplashPresenter @Inject constructor(val view: SplashContractor.View, val userPrefDataSource: UserPrefsDataSource) : SplashContractor.Presenter {

    @Inject
    override fun start() {

        userPrefDataSource.isAuthorizedAsObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                        onNext = { if (it) view.redirectToMain() else view.redirectToAuth() },
                        onError = { it.printStackTrace() }
                )
    }
}